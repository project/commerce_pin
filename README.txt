Commerce Pin
------------

Pin is an all-in-one hosted payment gateway for Australian businesses. Pin is
unique in the fact that it doesn't require any merchant account to be setup with
a banking institution.

Commerce Pin provides a payment method for the Drupal Commerce platform.
Configuration is as simple as:

 - Installing the module.
 - Entering your secret key.
 - Collect payments via your Pin account.

Pin supports token based recurring payments and customers, allowing Commerce
sites to shift the responsibility of storing credit cards while still being able
to bill its customers. In order to make the module fit in with the broader
Commerce ecosystem integrations with the following modules are planned:

 - Commerce Card on File
 - Commerce Recurring Framework

If you would like to contribute to the issue queue with any issues you may have
that would be great. If you have successfully collected a live payment, I would
also like to hear about it. I have only been able to put an order through a test
environment.
